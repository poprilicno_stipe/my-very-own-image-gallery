import React, { Component } from 'react';
import NavigationComponent from './components/navigation/NavigationComponent';
import SearchComponent from './components/search/SearchComponent';


class App extends Component {
  render() {
    return (
      <div>
        <NavigationComponent />
        <SearchComponent />
      </div>
    );
  }
}

export default App;
