import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Modal from '../modal/ModalComponent';
import styles from './image.css';


class ImageComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { isOpen: false };
        // states for displaying details and image in the modal
        this.state = { currentImg: '' };
        this.state = { currentImgLarge: '' };
        this.state = { currentImgTags: '' };
        this.state = { currentImgPhotograper: '' };
        this.state = { currentImgDownloads: '' };
        this.state = { currentImgSize: '' };
    }

    toggleModal = (img) => {
        this.setState({
            // toggle open and close states of the modal
            isOpen: !this.state.isOpen,
            // set states for displaying details and image in the modal
            currentImg: img.webformatURL,
            currentImgLarge: img.largeImageURL,
            currentImgTags: img.tags,
            currentImgPhotograper: img.user,
            currentImgViews: img.views,
            currentImgDownloads: img.download,
            currentImgSize: img.imageSize
        });
    }

    render() {
        let imageListContent;
        const { images } = this.props;
        // if images array is filled with recieved objects
        if (images) {
            imageListContent = (
                <div className="gallery">
                    {/* list out, or map out images form the images array */}
                    {images.map(img => (
                        <div className="container"
                            // open or close the modal on click
                            onClick={() => {
                                this.toggleModal(img)
                            }
                            }>
                            {/* display the image/s in smaller size */}
                            <img src={img.webformatURL} alt="" />
                            <div class="middle">
                                <div class="icon">+</div>
                            </div>
                        </div>
                    )
                    )}
                </div>
            )
        } else {
            imageListContent = null;
        }

        return (
            <div>
                {imageListContent}
                {/* modal */}
                <Modal show={this.state.isOpen} onClose={this.toggleModal}>
                    <img src={this.state.currentImgLarge} alt="" />
                    {/* footer contains additional details about the image/picture and the author */}
                    <footer>
                        <figcaption>tags: {this.state.currentImgTags}</figcaption>
                        <span>photographer: {this.state.currentImgPhotograper}</span>
                        <span>views: {this.state.currentImgViews}</span>
                        <span>downloads: {this.state.currentImgDownloads}</span>
                        <span>size: {this.state.currentImgSize}</span>
                    </footer>
                </Modal>
            </div>

        )
    }
}

ImageComponent.propTypes = {
    images: PropTypes.array.isRequired
}

export default ImageComponent;