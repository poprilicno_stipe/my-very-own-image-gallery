import React, { Component } from 'react';
import axios from 'axios';
import ImagesRecieved from '../image/ImageComponent';
import styles from './search.css';


class SearchComponent extends Component {
    state = {
        // search query
        searchInput: '',
        // number of images API returns to the client after the request
        numberOfImages: 48,
        // current page number
        page: 1,
        pixApiUrl: 'https://pixabay.com/api',
        pixApiKey: '8506502-fd530cf53ce4c9ff2733ae363',
        // array of objects returned by Pixabay API
        images: []
    }

    // method that handles search queries
    onTextChange = e => {
        // grab values entered into the search field
        const val = e.target.value;
        this.setState({ [e.target.name]: val }, () => {
            if (val === '') {
                // reset the states to the default values for a new search
                this.setState({ page: 1 });
                this.setState({ images: [] });
            } else {
                axios
                    // make a Pixabay API request using states values
                    .get(
                        `${this.state.pixApiUrl}/?key=${this.state.pixApiKey}&q=${this.state.searchInput}&image_type=photo&per_page=${this.state.numberOfImages}&page=${this.state.page}`
                    )
                    // if the request was succesful put the recived objects into the state images
                    .then((res) => {
                        this.setState({ images: res.data.hits })
                        console.log(res.data.hits)
                    })
                    // if the request was unsuccesful just log error to the console
                    .catch(err => console.log(err));
            }
        });
    };

    // make a search request to the Pixabay API for the next page by clicking the nextPageButton
    pageHandler = (pagingDirection) => {
        // temporary state page variable
        let tempStatePage;
        // determine if the nextPage or previousPage button was clicked and set the tempStatePage variable accordingly
        if (pagingDirection === "next") {
            tempStatePage = this.state.page + 1;
        } else {
            tempStatePage = this.state.page - 1;
        }
        // jump to the top of the page
        window.scrollTo(0, 0);
        // make a Pixabay API request using states values and tempStatePage value
        axios
            .get(
                `${this.state.pixApiUrl}/?key=${this.state.pixApiKey}&q=${this.state.searchInput}&image_type=photo&per_page=${this.state.numberOfImages}&page=${tempStatePage}`
            )
            // if the request was succesful put the recived objects into the state images
            .then(res => this.setState({ images: res.data.hits }))
            .then(
                () => {
                    // after the succesful request, determine if the nextPage or previousPage button was clicked and set the state page accordingly
                    if (pagingDirection === "next") {
                        this.setState({ page: this.state.page + 1 });
                    } else {
                        this.setState({ page: this.state.page - 1 });
                    }
                },
        )
            .catch(err => console.log(err),
                () => {
                    // after the unsuccesful request, determine if the nextPage or previousPage button was clicked and reset the state page accordingly
                    if (pagingDirection === "next") {
                        this.setState({ page: this.state.page - 1 });
                    } else {
                        this.setState({ page: this.state.page + 1 });
                    }
                }
            )
    }

    // use pageHandler method to access the previous page
    previousPage = () => {
        this.pageHandler("previous");
    };

    // use pageHandler method to access the next page
    nextPage = () => {
        this.pageHandler("next");
    };

    // render input, buttons
    render() {
        return (
            <div>
                {/* search input field */}
                <input 
                    id="search-box"
                    type="text"
                    name="searchInput"
                    placeholder="Search"
                    value={this.state.searchInput}
                    onChange={this.onTextChange}
                />
                {/* display search results */}
                {this.state.images.length > 0 ? (<figure><ImagesRecieved images={this.state.images} /></figure>) : null}
                {/* button for accesing previous page */}
                {this.state.page > 1 ? (<button label="Previous page" onClick={this.previousPage}>Previous page</button>) : null}
                {/* button for accesing next page */}
                {this.state.images.length > 0 ? (<button label="Next page" onClick={this.nextPage}>Next page</button>) : null}
            </div>
        );
    }
}

export default SearchComponent;