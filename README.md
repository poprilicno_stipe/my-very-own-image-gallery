# My Very Own Image Gallery

Simple React.js image gallery application that uses Pixabay API to search and show images. 
If You want to see some potential issues with this application please just scroll all way to the bottom of this README file.

## Requirements

For development, you will need Node.js, React.js, decent text editor (for example, I used Visual Studio Code).

### Node

[Node](http://nodejs.org/) is really easy to install & now includes [NPM](https://npmjs.org/).
Now, You can run these commands:

    $ node --version
    v9.8.0

    $ npm --version
    5.6.0

#### Node installation on Windows

You can find node at [official Node.js website](http://nodejs.org/).
Also, be sure to have `git` available in your PATH, `npm` might need it.

---

## Install

    $ git clone https://bitbucket.org/poprilicno_stipe/my-very-own-image-gallery/
    $ cd my-very-own-image-gallery
    $ npm install

## Start & watch

    $ npm start

## Simple build for production

    $ npm run build

## Update sources

Some packages usages might change (or maybe I will just make an advanced version, place it in a new repository, and keep it hidden like I usually do for some reason) so you should run `npm prune` & `npm install` often.
A common way to update is by doing

    $ git pull
    $ npm prune
    $ npm install

To run those 3 commands you can just do

    $ npm run pull

---

## Languages & tools

### HTML

### CSS

### JavaScript

- [React](http://facebook.github.io/react) of course. Basically, the whole application is made with React.

### React specific libraries and modules used

- [ReactDOM](https://reactjs.org/docs/react-dom.html), [axios](https://github.com/axios/axios), [prop-types](https://reactjs.org/docs/typechecking-with-proptypes.html) and [registerServiceWorker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API)

### My Very Own Critical Review of My Very Own Image Gallery (01.05.2018)

- I am not fully satisfied with the responsibilites of my components. SearchComponent seems a bit bloated and maybe it's stealing some capabilites better suited for ImageComponent.

- Image gallery grid/tiles of images I made would better fit "infinite load" (load on scroll) or "load more" (load on button click) way of accesing the next page. However I dislike the idea of loading huge amounts of images and keeping them in the browser memory. Maybe combining "infinite load" with paging would be more sensible. Load more, load more, load more then load the next page, load more, load more, load more and then load the next page...

- Buttons "Previous page" and "Next page" are logically positioned at the bottom of the page (they appear after the search), "Next page" being on the right, "Previous page" being on the left. So why did I put the "Next page" button at the bottom center of the page (only on the first page, as there is no "Previous page" then)? This will tire the user and force him to adapt when the "Previous page" appears and the "Next page" button moves further right.

- "Next page" button does not disappear when there is no next page. But making two calls, and one just to determine if there is a next page is something I chose not to do for application this light/small, just too much traffic for something like this. I might be forced to do it to make user experience better, though.

- I dislike the way I did the "second view" modal/dialog that gives us the details and information about the image. I will need to look into: more of the actual image and less of the text/info and empty space.

- Performance needs to be better (images loading being the main issue). Maybe using [previewURL](https://pixabay.com/api/docs/) vs. [webformatURL](https://pixabay.com/api/docs/) that I am currently using.

- There are no comments in CSS files. They are simple and short but still - there aren't any comments.
